package com.tutorials.spring.profiles;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alecu.tutorials.spring.profiles.SpringProfilesApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SpringProfilesApplication.class)
public class SpringProfilesApplicationTests {

	@Test
	public void contextLoads() {
	}

}
