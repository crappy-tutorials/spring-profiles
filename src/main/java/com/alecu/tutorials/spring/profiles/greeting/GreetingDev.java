package com.alecu.tutorials.spring.profiles.greeting;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("dev")
public class GreetingDev implements Greeting {

    @Override
    public String getGreetingTitle() {
        return "[DEV] Hello";
    }

}
