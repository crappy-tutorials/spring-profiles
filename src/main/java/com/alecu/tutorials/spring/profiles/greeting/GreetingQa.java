package com.alecu.tutorials.spring.profiles.greeting;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("qa")
public class GreetingQa implements Greeting {

    @Override
    public String getGreetingTitle() {
        return "[QA] Hello";
    }

}
