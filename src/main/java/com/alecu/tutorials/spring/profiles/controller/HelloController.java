package com.alecu.tutorials.spring.profiles.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alecu.tutorials.spring.profiles.greeting.Greeting;

@RestController
public class HelloController {
    
    @Autowired Greeting greeting;

    @RequestMapping("/hello")
    public ResponseEntity<String> simpleGreeting(
            @RequestParam(value = "name", required = false) String name
            ) {

        String result = String.format("%s %s!", greeting.getGreetingTitle(), (name == null ? "world" : name));
        return new ResponseEntity<String>(result, HttpStatus.OK);
    }

    @RequestMapping("/hello/rest")
    public ResponseEntity<Map<Object, Object>> restGreeting(
            @RequestParam(value = "name", required = false) String name
            ) {

        Map<Object, Object> result = new HashMap<Object, Object>();
        if (name == null) {
            result.put("value", "Hello world!");
        } else {
            result.put("value", String.format("Hello %s!", name));
        }

        return new ResponseEntity<Map<Object, Object>>(result, HttpStatus.OK);
    }
}
