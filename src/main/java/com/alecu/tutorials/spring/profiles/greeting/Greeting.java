package com.alecu.tutorials.spring.profiles.greeting;

public interface Greeting {
    public String getGreetingTitle();
}
